/* Test snprintf and vsnprintf */

#include <assert.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>

#ifdef TEST_VSNPRINTF
#define snprintf our_snprintf

int our_snprintf(char *buf, size_t size, const char *fmt, ...) {
    va_list ap;
    int r;

    va_start(ap, fmt);
    r = vsnprintf(buf, size, fmt, ap);
    va_end(ap);

    return r;
}
#endif

#define setup() \
    do { \
        printf("Testing line %d... ", __LINE__); \
        fflush(stdout); \
        memset(buf, 'X', sizeof buf); \
    } while (0)

#define check(total, value) \
    do { \
        size_t actual = strlen(value); \
        assert(r == total); \
        assert(buf[actual] == '\0'); \
        for (i = actual + 1; i < sizeof buf; i++) \
            assert(buf[i] == 'X'); \
        assert(strcmp(buf, value) == 0); \
        printf("OK\n"); \
    } while (0)

int main(int argc, char *argv[]) {
    int size = 10;
    char buf[15];
    int i, r;

    /* Simple tests */

    setup();
    r = snprintf(buf, size, "");
    check(0, "");

    setup();
    r = snprintf(buf, size, "hello");
    check(5, "hello");

    /* Test overrunning the buffer in various ways */

    setup();
    r = snprintf(buf, 4, "%s", "verylong");
    check(8, "ver");

    setup();
    r = snprintf(buf, 4, "%8s", "x");
    check(8, "   ");

    setup();
    r = snprintf(buf, 4, "%-8s", "x");
    check(8, "x  ");

    setup();
    r = snprintf(buf, 4, "%d", 12345678);
    check(8, "123");

    setup();
    r = snprintf(buf, 4, "%08d", 42);
    check(8, "000");

    setup();
    r = snprintf(buf, 4, "%-8d", 42);
    check(8, "42 ");

    setup();
    r = snprintf(buf, 4, "%s%d", "xxx", -42);
    check(6, "xxx");

    setup();
    r = snprintf(buf, 4, "%x", 0xabcd1234);
    check(8, "abc");

    /* Test size == 0 behaviour as defined by C99 (not pre-C99 SuS) */

    setup();
    r = snprintf(NULL, 0, "%s", "hello");
    assert(r == 5);
    printf("OK\n");

    setup();
    r = snprintf(buf, 0, "%s", "hello");
    assert(r == 5);
    for (i = 0; i < sizeof buf; i++)
        assert(buf[i] == 'X');
    printf("OK\n");

#ifdef TEST_VSNPRINTF
    printf("vsnprintf OK\n");
#else
    printf("snprintf OK\n");
#endif
    return 0;
}
