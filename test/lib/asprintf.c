/* Test asprintf and vasprintf */

#include <assert.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef TEST_VASPRINTF
#define asprintf our_asprintf

int our_asprintf(char **bufptr, const char *fmt, ...) {
    va_list ap;
    size_t r;

    va_start(ap, fmt);
    r = vasprintf(bufptr, fmt, ap);
    va_end(ap);

    return r;
}
#endif

#define setup() \
    do { \
        printf("Testing line %d... ", __LINE__); \
        fflush(stdout); \
        buf = NULL; \
    } while (0)

#define check(total, value) \
    do { \
        size_t actual = strlen(value); \
        assert(r == total); \
        assert(buf != NULL); \
        assert(buf[actual] == '\0'); \
        assert(strcmp(buf, value) == 0); \
        free(buf); \
        printf("OK\n"); \
    } while (0)

int main(int argc, char *argv[]) {
    char *buf;
    int r;

    /* Simple tests (we can't easily test for failure) */

    setup();
    r = asprintf(&buf, "");
    check(0, "");

    setup();
    r = asprintf(&buf, "hello");
    check(5, "hello");

    setup();
    r = asprintf(&buf, "longer %04d %s", 42, "string");
    check(18, "longer 0042 string");

#ifdef TEST_VASPRINTF
    printf("vasprintf OK\n");
#else
    printf("asprintf OK\n");
#endif
    return 0;
}
