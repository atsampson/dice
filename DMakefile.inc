# (c)Copyright 1992-2023 Obvious Implementations Corp.

# Each subdirectory that include this file must define MODULE, and can define
# one of EXE, TOOLS or LIB, plus custom all/install rules.

## Type of module

.ifdef EXE
# One executable built from multiple sources
HASEXE= yes
INSTEXES= yes
.endif
.ifdef TOOLS
# Multiple executables built from one source each
HASTOOLS= yes
INSTEXES= yes
.endif
.ifdef LIB
# Library built from multiple sources
HASLIB= yes
.endif

SRCS ?=
EXE ?=
TOOLS ?=
LIB ?=

## OS-specific paths and tools

.ifos Amiga

# Amiga native build

TOPSRCDIR ?= /
INSTDIR ?= DCC:
OBJDIR ?= DOBJ:
BINDIR ?= XDBIN:

MAKEPROTO ?= makeproto -o
CC ?= dcc
CFLAGS ?= -2.0 -proto -ms -r -mRR
DEFINES ?= -DREGISTERED -DCOMMERCIAL -DDEBUG
ASM = yes
LDFLAGS ?=
LDLIBS ?=

DMAKE ?= dmake
ECHO ?= echo
MAKEDIR ?= makedir
DELETE ?= delete all
RENAME ?= rename

PSRCS= $(SRCS)

.else

# Build on Unix

TOPSRCDIR ?= ..
INSTDIR ?= /usr/local/dice/

.ifdef TARGET_AMIGA

# Crossbuild from Unix for Amiga

OBJDIR ?= $(TOPSRCDIR)/aobj/
BINDIR ?= $(TOPSRCDIR)/abin/

MAKEPROTO ?= makeproto -o
CC ?= dcc
CFLAGS ?= -2.0 -proto -ms -r -unixrc
DEFINES ?= -DREGISTERED -DCOMMERCIAL -DDEBUG -DBUILD_UNIX
ASM = yes
AR ?= unused
RANLIB ?= unused
LDFLAGS ?= -L$(OBJDIR)
LDLIBS ?=

.else

# Unix native build

OBJDIR ?= $(TOPSRCDIR)/uobj/
BINDIR ?= $(TOPSRCDIR)/ubin/

# This only works for prototypes in .c files
MAKEPROTO ?= grep -h -E '^Prototype' >
CC ?= cc
CFLAGS ?= -O2 -g -fwrapv -fno-strict-aliasing -Wall -Wstrict-prototypes -Werror
DEFINES ?= -DREGISTERED -DCOMMERCIAL -DDEBUG -DNO_ASM -DINTELBYTEORDER -DCTOD -DBUILD_UNIX
AR ?= ar
RANLIB ?= ranlib
LDFLAGS ?= -L$(OBJDIR)
LDLIBS ?= -lamiga

.endif

DMAKE ?= dxmake
ECHO ?= echo
MAKEDIR ?= mkdir
DELETE ?= rm -rf
RENAME ?= mv

.endif

## Tool options

ALLCFLAGS= $(CFLAGS) -I$(OBJDIR) -I$(TOPSRCDIR) -D_INSTDIR=$(INSTDIR) $(DEFINES)

## Targets and rules

PROTOS ?= $(OBJDIR)$(MODULE)-protos.h
.ifdef ASM
OBJS= $(SRCS:"*.?":"$(OBJDIR)$(MODULE)_*.o")
.else
OBJS= $(SRCS:"*.c":"$(OBJDIR)$(MODULE)_*.o")
.endif
EXES ?= $(EXE:"*":"$(BINDIR)*") $(TOOLS:"*":"$(BINDIR)*")
LIBS ?= $(LIB:"*":"$(OBJDIR)*")
ALL ?= $(EXES) $(LIBS)

all: $(ALL)

$(OBJDIR) : $(OBJDIR)
    $(MAKEDIR) %(left:"*/":"*")

$(BINDIR) : $(BINDIR)
    $(MAKEDIR) %(left:"*/":"*")

$(PROTOS) : $(OBJDIR)

$(PROTOS) : $(SRCS)
    -$(DELETE) %(left) %(left).new
    -$(MAKEPROTO) %(left).new %(right)
    $(RENAME) %(left).new %(left)

$(OBJS) : $(OBJDIR)

.ifdef HASEXE

$(OBJS) : $(PROTOS)

.endif

$(SRCS:"*.?":"$(OBJDIR)$(MODULE)_*.o") : $(SRCS)
    $(CC) $(ALLCFLAGS) -c -o %(left) %(right)

.ifdef HASEXE

$(BINDIR)$(EXE) : $(BINDIR)

LINKBEFORE ?=
LINKAFTER ?=

$(BINDIR)$(EXE) : $(OBJS)
    $(CC) $(CFLAGS) $(LDFLAGS) -o %(left) $(LINKBEFORE) %(right) $(LINKAFTER) $(LDLIBS)

.endif

.ifdef HASTOOLS

$(TOOLS:"*":"$(BINDIR)*") : $(BINDIR)

$(TOOLS:"*":"$(BINDIR)*") : $(SRCS)
    $(CC) $(ALLCFLAGS) $(LDFLAGS) -o %(left) %(right) $(LDLIBS)

.endif

.ifdef HASLIB

$(OBJDIR)$(LIB) : $(OBJDIR)

$(OBJDIR)$(LIB) : $(OBJS)
    -$(DELETE) %(left) %(left).new
    $(AR) cr %(left).new %(right)
    $(RANLIB) %(left).new
    $(RENAME) %(left).new %(left)

.endif

INSTSUBDIRS= bin config dlib include

$(INSTSUBDIRS:"*":"$(INSTDIR)*") : $(INSTSUBDIRS:"*":"$(INSTDIR)*")
    $(MAKEDIR) %(left)

install:

.ifdef INSTEXES
.ifos Amiga
.else

install: $(INSTDIR)bin $(EXES) phony
    install -m755 $(EXES) $(INSTDIR)bin

.endif
.endif

clean: phony
    -$(DELETE) $(PROTOS) $(OBJS) $(EXES) $(LIBS)

## Subdir targets

.ifdef SUBDIRS

SUBDMAKE= $(DMAKE) -q \
    "-DINSTDIR=$(INSTDIR)" \
    "-DDEFINES=$(DEFINES)"

.ifdef TARGET_AMIGA
SUBDMAKE= $(SUBDMAKE) "-DTARGET_AMIGA"
.endif

all: $(SUBDIRS:"*":"all-*")

$(SUBDIRS:"*":"all-*") : phony

$(SUBDIRS:"*":"all-*") : $(SUBDIRS)
    @cd %(right)
    @$(ECHO) "Build %(right)"
    @$(SUBDMAKE) all
    @cd

install: $(SUBDIRS:"*":"install-*")

$(SUBDIRS:"*":"install-*") : phony

$(SUBDIRS:"*":"install-*") : $(SUBDIRS)
    @cd %(right)
    @$(ECHO) "Install %(right)"
    @$(SUBDMAKE) install
    @cd

clean: $(SUBDIRS:"*":"clean-*")

$(SUBDIRS:"*":"clean-*") : phony

$(SUBDIRS:"*":"clean-*") : $(SUBDIRS)
    @cd %(right)
    @$(ECHO) "Clean %(right)"
    @$(SUBDMAKE) clean
    @cd

.endif

# Depend on this target to make a target unconditional
phony:
