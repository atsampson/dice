
/*
 * $VER: inttypes.h 1.0 (29.10.2023)
 *
 * (c)Copyright 2023 Obvious Implementations Corp, All Rights Reserved
 */

#ifndef INTTYPES_H
#define INTTYPES_H

/*
 *  This is a subset of C99's <inttypes.h> header.
 *  The SCN constants are not implemented.
 */

#include <stdint.h>

#define PRId8 "d"
#define PRId16 "d"
#define PRId32 "d"
#define PRIdMAX "d"
#define PRIdPTR "d"

#define PRIi8 "i"
#define PRIi16 "i"
#define PRIi32 "i"
#define PRIiMAX "i"
#define PRIiPTR "i"

#define PRIu8 "u"
#define PRIu16 "u"
#define PRIu32 "u"
#define PRIuMAX "u"
#define PRIuPTR "u"

#define PRIo8 "o"
#define PRIo16 "o"
#define PRIo32 "o"
#define PRIoMAX "o"
#define PRIoPTR "o"

#define PRIx8 "x"
#define PRIx16 "x"
#define PRIx32 "x"
#define PRIxMAX "x"
#define PRIxPTR "x"

#define PRIX8 "X"
#define PRIX16 "X"
#define PRIX32 "X"
#define PRIXMAX "X"
#define PRIXPTR "X"

#endif

