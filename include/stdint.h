
/*
 * $VER: stdint.h 1.0 (29.10.2023)
 *
 * (c)Copyright 2023 Obvious Implementations Corp, All Rights Reserved
 */

#ifndef STDINT_H
#define STDINT_H

/*
 *  This is a subset of C99's <stdint.h> header.
 *  The _fast and _least types are not implemented.
 *  INT64_WIDTH etc. are not defined, as we don't have a 64-bit type.
 */

typedef signed char int8_t;
#define INT8_WIDTH 8
#define INT8_MIN -128
#define INT8_MAX 127

typedef signed short int16_t;
#define INT16_WIDTH 16
#define INT16_MIN -32768
#define INT16_MAX 32767

typedef signed long int32_t;
#define INT32_WIDTH 32
#define INT32_MIN -2147483648L
#define INT32_MAX 2147483647L

typedef unsigned char uint8_t;
#define UINT8_WIDTH 8
#define UINT8_MIN 0
#define UINT8_MAX 255

typedef unsigned short uint16_t;
#define UINT16_WIDTH 16
#define UINT16_MIN 0
#define UINT16_MAX 65535

typedef unsigned long uint32_t;
#define UINT32_WIDTH 32
#define UINT32_MIN 0UL
#define UINT32_MAX 4294967295UL

typedef signed long intmax_t;
#define INTMAX_WIDTH INT32_WIDTH
#define INTMAX_MIN INT32_MIN
#define INTMAX_MAX INT32_MAX
#define INTMAX_C(val) val ## L

typedef unsigned long uintmax_t;
#define UINTMAX_WIDTH UINT32_WIDTH
#define UINTMAX_MIN UINT32_MIN
#define UINTMAX_MAX UINT32_MAX
#define UINTMAX_C(val) val ## UL

typedef signed long intptr_t;
#define INTPTR_WIDTH INT32_WIDTH
#define INTPTR_MIN INT32_MIN
#define INTPTR_MAX INT32_MAX

typedef unsigned long uintptr_t;
#define UINTPTR_WIDTH UINT32_WIDTH
#define UINTPTR_MIN UINT32_MIN
#define UINTPTR_MAX UINT32_MAX

#endif
