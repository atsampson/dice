/* $VER: ../include/pragmas/nonvolatile_pragmas.h 1.0 (23.10.2023) */
#ifndef NVBase_PRAGMA_H
#define NVBase_PRAGMA_H

#pragma libcall NVBase GetCopyNV 1e 19803
#pragma libcall NVBase FreeNVData 24 801
#pragma libcall NVBase StoreNV 2a 10a9805
#pragma libcall NVBase DeleteNV 30 19803
#pragma libcall NVBase GetNVInfo 36 101
#pragma libcall NVBase GetNVList 3c 1802
#pragma libcall NVBase SetNVProtection 42 129804

#endif
