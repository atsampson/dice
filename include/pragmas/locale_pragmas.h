/* $VER: ../include/pragmas/locale_pragmas.h 1.0 (23.10.2023) */
#ifndef LocaleBase_PRAGMA_H
#define LocaleBase_PRAGMA_H

#pragma libcall LocaleBase CloseCatalog 24 801
#pragma libcall LocaleBase CloseLocale 2a 801
#pragma libcall LocaleBase ConvToLower 30 0802
#pragma libcall LocaleBase ConvToUpper 36 0802
#pragma libcall LocaleBase FormatDate 3c ba9804
#pragma libcall LocaleBase FormatString 42 ba9804
#pragma libcall LocaleBase GetCatalogStr 48 90803
#pragma libcall LocaleBase GetLocaleStr 4e 0802
#pragma libcall LocaleBase IsAlNum 54 0802
#pragma libcall LocaleBase IsAlpha 5a 0802
#pragma libcall LocaleBase IsCntrl 60 0802
#pragma libcall LocaleBase IsDigit 66 0802
#pragma libcall LocaleBase IsGraph 6c 0802
#pragma libcall LocaleBase IsLower 72 0802
#pragma libcall LocaleBase IsPrint 78 0802
#pragma libcall LocaleBase IsPunct 7e 0802
#pragma libcall LocaleBase IsSpace 84 0802
#pragma libcall LocaleBase IsUpper 8a 0802
#pragma libcall LocaleBase IsXDigit 90 0802
#pragma libcall LocaleBase OpenCatalogA 96 a9803
#pragma libcall LocaleBase OpenLocale 9c 801
#pragma libcall LocaleBase ParseDate a2 ba9804
#pragma libcall LocaleBase StrConvert ae 10a9805
#pragma libcall LocaleBase StrnCmp b4 10a9805

#endif
