/* $VER: ../include/pragmas/amigaguide_pragmas.h 1.0 (23.10.2023) */
#ifndef AmigaGuideBase_PRAGMA_H
#define AmigaGuideBase_PRAGMA_H

#pragma libcall AmigaGuideBase LockAmigaGuideBase 24 801
#pragma libcall AmigaGuideBase UnlockAmigaGuideBase 2a 001
#pragma libcall AmigaGuideBase OpenAmigaGuideA 36 9802
#pragma libcall AmigaGuideBase OpenAmigaGuideAsyncA 3c 0802
#pragma libcall AmigaGuideBase CloseAmigaGuide 42 801
#pragma libcall AmigaGuideBase AmigaGuideSignal 48 801
#pragma libcall AmigaGuideBase GetAmigaGuideMsg 4e 801
#pragma libcall AmigaGuideBase ReplyAmigaGuideMsg 54 801
#pragma libcall AmigaGuideBase SetAmigaGuideContextA 5a 10803
#pragma libcall AmigaGuideBase SendAmigaGuideContextA 60 0802
#pragma libcall AmigaGuideBase SendAmigaGuideCmdA 66 10803
#pragma libcall AmigaGuideBase SetAmigaGuideAttrsA 6c 9802
#pragma libcall AmigaGuideBase GetAmigaGuideAttr 72 98003
#pragma libcall AmigaGuideBase LoadXRef 7e 9802
#pragma libcall AmigaGuideBase ExpungeXRef 84 00
#pragma libcall AmigaGuideBase AddAmigaGuideHostA 8a 90803
#pragma libcall AmigaGuideBase RemoveAmigaGuideHostA 90 9802
#pragma libcall AmigaGuideBase GetAmigaGuideString d2 001

#endif
