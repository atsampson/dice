/* $VER: ../include/pragmas/datatypes_pragmas.h 1.0 (23.10.2023) */
#ifndef DataTypesBase_PRAGMA_H
#define DataTypesBase_PRAGMA_H

#pragma libcall DataTypesBase ObtainDataTypeA 24 98003
#pragma libcall DataTypesBase ReleaseDataType 2a 801
#pragma libcall DataTypesBase NewDTObjectA 30 8002
#pragma libcall DataTypesBase DisposeDTObject 36 801
#pragma libcall DataTypesBase SetDTAttrsA 3c ba9804
#pragma libcall DataTypesBase GetDTAttrsA 42 a802
#pragma libcall DataTypesBase AddDTObject 48 0a9804
#pragma libcall DataTypesBase RefreshDTObjectA 4e ba9804
#pragma libcall DataTypesBase DoAsyncLayout 54 9802
#pragma libcall DataTypesBase DoDTMethodA 5a ba9804
#pragma libcall DataTypesBase RemoveDTObject 60 9802
#pragma libcall DataTypesBase GetDTMethods 66 801
#pragma libcall DataTypesBase GetDTTriggerMethods 6c 801
#pragma libcall DataTypesBase PrintDTObjectA 72 ba9804
#pragma libcall DataTypesBase GetDTString 8a 001

#endif
