/* $VER: ../include/pragmas/bullet_pragmas.h 1.0 (23.10.2023) */
#ifndef BulletBase_PRAGMA_H
#define BulletBase_PRAGMA_H

#pragma libcall BulletBase OpenEngine 1e 00
#pragma libcall BulletBase CloseEngine 24 801
#pragma libcall BulletBase SetInfoA 2a 9802
#pragma libcall BulletBase ObtainInfoA 30 9802
#pragma libcall BulletBase ReleaseInfoA 36 9802

#endif
