/* $VER: ../include/pragmas/colorwheel_pragmas.h 1.0 (23.10.2023) */
#ifndef ColorWheelBase_PRAGMA_H
#define ColorWheelBase_PRAGMA_H

#pragma libcall ColorWheelBase ConvertHSBToRGB 1e 9802
#pragma libcall ColorWheelBase ConvertRGBToHSB 24 9802

#endif
