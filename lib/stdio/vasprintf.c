
/*
 *  VASPRINTF.C
 *
 *    (c)Copyright 1992-2023 Obvious Implementations Corp.  Redistribution and
 *    use is allowed under the terms of the DICE-LICENSE FILE,
 *    DICE-LICENSE.TXT.
 */

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <lib/misc.h>

#ifndef HYPER
#define HYPER(x) x
#endif

int
HYPER(vasprintf)(bufptr, ctl, va)
char **bufptr;
const char *ctl;
va_list va;
{
    int size, error;
    va_list ova;

    va_copy(ova, va);

    size = vsnprintf(NULL, 0, ctl, va);
    if (size < 0) {
	va_end(ova);
	*bufptr = NULL;
	return(-1);
    }

    *bufptr = malloc(size + 1);
    if (*bufptr == NULL) {
	va_end(ova);
	return(-1);
    }

    error = vsnprintf(*bufptr, size + 1, ctl, ova);
    va_end(ova);
    if (error != size) {
	free(*bufptr);
	*bufptr = NULL;
	return(-1);
    }

    return(error);
}

