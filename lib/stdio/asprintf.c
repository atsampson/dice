
/*
 *  ASPRINTF.C
 *
 *    (c)Copyright 1992-2023 Obvious Implementations Corp.  Redistribution and
 *    use is allowed under the terms of the DICE-LICENSE FILE,
 *    DICE-LICENSE.TXT.
 */

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <lib/misc.h>

#ifndef HYPER
#define HYPER(x) x
#endif

int
HYPER(asprintf)(bufptr, ctl, ...)
char **bufptr;
const char *ctl;
{
    int size, error;
    va_list va;

    va_start(va, ctl);
    size = vsnprintf(NULL, 0, ctl, va);
    va_end(va);
    if (size < 0) {
	*bufptr = NULL;
	return(-1);
    }

    *bufptr = malloc(size + 1);
    if (*bufptr == NULL)
	return(-1);

    va_start(va, ctl);
    error = vsnprintf(*bufptr, size + 1, ctl, va);
    va_end(va);
    if (error != size) {
	free(*bufptr);
	*bufptr = NULL;
	return(-1);
    }

    return(error);
}

