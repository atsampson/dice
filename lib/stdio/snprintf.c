
/*
 *  SNPRINTF.C
 *
 *    (c)Copyright 1992-2023 Obvious Implementations Corp.  Redistribution and
 *    use is allowed under the terms of the DICE-LICENSE FILE,
 *    DICE-LICENSE.TXT.
 */

#include <stdarg.h>
#include <stdio.h>
#include <lib/misc.h>

#ifndef HYPER
#define HYPER(x) x
#endif

struct StringDest {
    char *p;
    size_t space;
    size_t count;
};

static unsigned int
_snwrite(buf, n1, n2, dest)
char *buf;
size_t n1;
size_t n2;
struct StringDest *dest;
{
    size_t n;

    if (n1 == 1)
	n = n2;
    else if (n2 == 1)
	n = n1;
    else
	n = n1 * n2;

    dest->count += n;

    if (dest->space == 0)
	return(n2);
    if (n > dest->space)
	n = dest->space;

    _slow_bcopy(buf, dest->p, n);

    dest->p += n;
    dest->space -= n;

    return(n2);
}

int
HYPER(snprintf)(buf, size, ctl, ...)
char *buf;
size_t size;
const char *ctl;
{
    struct StringDest dest;
    int error;
    va_list va;

    dest.p = buf;
    dest.space = size ? size - 1 : 0;
    dest.count = 0;

    va_start(va, ctl);
    error = _pfmt(ctl, va, _snwrite, &dest);
    va_end(va);

    if (buf && dest.p < buf + size)
	*dest.p = 0;

    if (error < 0)
	return(error);
    else
	return(dest.count);
}

